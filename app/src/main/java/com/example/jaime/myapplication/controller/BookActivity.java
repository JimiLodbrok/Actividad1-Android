package com.example.jaime.myapplication.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jaime.myapplication.R;
import com.example.jaime.myapplication.model.DataHolder;

public class BookActivity extends AppCompatActivity implements EventsAdminListener {

    private EditText edtxt_title;
    private TextView libro;

    private Button btn_next;
    private Button btn_previous;
    private Button btn_perfil;

    private EventsAdmin evento;
    private int puntero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        evento = new EventsAdmin(this);
        initComponents();
        loadData();
    }

    public void initComponents() {
        edtxt_title = (EditText) findViewById(R.id.edtxt_title);
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_previous = (Button) findViewById(R.id.btn_previous);
        btn_perfil = (Button) findViewById(R.id.btn_volver);
        libro = (TextView) findViewById(R.id.libro);
        btn_next.setOnClickListener(evento);
        btn_previous.setOnClickListener(evento);
        btn_perfil.setOnClickListener(evento);
        puntero = DataHolder.getData().getPuntero();
    }

    public void loadData() {
        if (puntero == 0) {
            btn_previous.setEnabled(false);
            btn_next.setEnabled(true);
        } else if (puntero == getResources().getStringArray(R.array.tituloLibro).length - 1) {
            btn_previous.setEnabled(true);
            btn_next.setEnabled(false);
        } else {
            btn_previous.setEnabled(true);
            btn_next.setEnabled(true);
        }
        edtxt_title.setText(getResources().getStringArray(R.array.tituloLibro)[puntero]);
        libro.setText(getResources().getStringArray(R.array.libro)[puntero]);
    }

    @Override
    public void touchScreen(View view) {
        if (view.getId() == R.id.btn_next) {
            puntero++;
            loadData();
        } else if (view.getId() == R.id.btn_previous) {
            puntero--;
            loadData();
        } else if (view.getId() == R.id.btn_volver) {
            DataHolder.getData().setPuntero(puntero);
            finish();
        }
    }
}
