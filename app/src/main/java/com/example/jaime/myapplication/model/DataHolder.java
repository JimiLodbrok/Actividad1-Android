package com.example.jaime.myapplication.model;

/**
 * Created by Jaime on 14/11/2017.
 */

public class DataHolder {
    private static DataHolder methods = new DataHolder();
    private String name;
    private String email;
    private String tlf;
    private String dir;
    private int year, month, day, puntero;

    public DataHolder() {
        name = "";
        email = "";
        tlf = "";
        dir = "";
        month = 0;
        year = 0;
        day = 0;
        puntero = 0;

    }

    public int getPuntero() {
        return puntero;
    }

    public void setPuntero(int puntero) {
        this.puntero = puntero;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public static DataHolder getData() {
        return methods;
    }
}