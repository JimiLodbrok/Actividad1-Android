package com.example.jaime.myapplication.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.example.jaime.myapplication.R;
import com.example.jaime.myapplication.model.DataHolder;


public class PerfilActivity extends AppCompatActivity implements EventsAdminListener {

    private TextInputEditText edtxt_name;
    private TextInputEditText edtxt_email;
    private TextInputEditText edtxt_tlf;
    private TextInputEditText edtxt_dir;
    private DatePicker dp_date;

    private Button btn_edit;
    private Button btn_nextAct;

    private EventsAdmin evento;
    private boolean editMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        evento = new EventsAdmin(this);
        initComponents();
        loadData();
    }

    public void initComponents() {
        edtxt_name = (TextInputEditText) findViewById(R.id.edtxt_name);
        edtxt_email = (TextInputEditText) findViewById(R.id.edtxt_email);
        edtxt_tlf = (TextInputEditText) findViewById(R.id.edtxt_tlf);
        edtxt_dir = (TextInputEditText) findViewById(R.id.edtxt_dir);
        dp_date = (DatePicker) findViewById(R.id.dp_date);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        btn_nextAct = (Button) findViewById(R.id.btn_nextAct);
        btn_edit.setOnClickListener(evento);
        btn_nextAct.setOnClickListener(evento);
        setNoFocus();
    }


    public TextInputEditText getEdtxt_name() {
        return edtxt_name;
    }

    public TextInputEditText getEdtxt_email() {
        return edtxt_email;
    }

    public TextInputEditText getEdtxt_tlf() {
        return edtxt_tlf;
    }

    public TextInputEditText getEdtxt_dir() {
        return edtxt_dir;
    }

    public DatePicker getDp_date() {
        return dp_date;
    }

    public Button getBtn_edit() {
        return btn_edit;
    }

    public boolean isEditable() {
        return editMode;
    }

    @Override
    public void touchScreen(View view) {
        if (view.getId() == R.id.btn_edit) {
            if (!isEditable()) {
                setFocus();
            } else if (getBtn_edit().getText().equals(R.string.edit2)) {
                setFocus();
                saveData();
            } else {
                setNoFocus();
            }
        } else if (view.getId() == R.id.btn_nextAct) {
            launchSecondActivity();
        }
    }

    public void setNoFocus() {
        getEdtxt_name().setEnabled(false);
        getEdtxt_tlf().setEnabled(false);
        getEdtxt_dir().setEnabled(false);
        getEdtxt_email().setEnabled(false);
        getDp_date().setClickable(false);
        getEdtxt_name().setFocusableInTouchMode(false);
        getEdtxt_tlf().setFocusableInTouchMode(false);
        getEdtxt_dir().setFocusableInTouchMode(false);
        getEdtxt_email().setFocusableInTouchMode(false);
        getBtn_edit().setText(R.string.edit);
        editMode = false;
    }

    public void setFocus() {
        getEdtxt_name().setEnabled(true);
        getEdtxt_tlf().setEnabled(true);
        getEdtxt_dir().setEnabled(true);
        getEdtxt_email().setEnabled(true);
        getDp_date().setClickable(true);
        getEdtxt_name().setFocusableInTouchMode(true);
        getEdtxt_tlf().setFocusableInTouchMode(true);
        getEdtxt_dir().setFocusableInTouchMode(true);
        getEdtxt_email().setFocusableInTouchMode(true);
        getBtn_edit().setText(R.string.edit2);
        editMode = true;
    }

    public void launchSecondActivity() {
        Intent bookActivity = new Intent(this, BookActivity.class);
        startActivity(bookActivity);
    }

    public void loadData() {
        getEdtxt_name().setText(DataHolder.getData().getName());
        getEdtxt_email().setText(DataHolder.getData().getEmail());
        getEdtxt_tlf().setText(DataHolder.getData().getTlf());
        getEdtxt_dir().setText(DataHolder.getData().getDir());
        getDp_date().updateDate(DataHolder.getData().getYear(), DataHolder.getData().getMonth(), DataHolder.getData().getDay());
    }

    public void saveData() {
        DataHolder.getData().setName(getEdtxt_name().getText().toString());
        DataHolder.getData().setEmail(getEdtxt_email().getText().toString());
        DataHolder.getData().setTlf(getEdtxt_tlf().getText().toString());
        DataHolder.getData().setDir(getEdtxt_dir().getText().toString());
        DataHolder.getData().setYear(getDp_date().getYear());
        DataHolder.getData().setMonth(getDp_date().getMonth());
        DataHolder.getData().setDay(getDp_date().getDayOfMonth());
    }
}
