package com.example.jaime.myapplication.controller;

import android.view.View;

/**
 * Created by Jaime on 15/11/2017.
 */

public class EventsAdmin implements View.OnClickListener {

    private EventsAdminListener act;

    public EventsAdmin(EventsAdminListener act) {
        this.act = act;
    }
    @Override
    public void onClick(View view) {
        act.touchScreen(view);
    }
}

interface EventsAdminListener {
    void touchScreen(View view);
}